import {Component, NgModule, OnInit, ViewChild} from '@angular/core';
import { XmlService } from 'src/app/out/service/xml.service';
import { ActivatedRoute } from '@angular/router';
import { Timeline} from "primeng/timeline";
import { DataService } from 'src/app/out/service/data.service';


class Timer {
  private _minutes: number = 0;
  private _secondes: number = 0;
  private _totalSecondes: number = 0;
  private _timer: any;
  get minutes(): number { return this._minutes; }
  get secondes(): number { return this._secondes; }
  start() {
    this._timer = setInterval(() => {
      this._minutes = Math.floor(--this._totalSecondes / 60);
      this._secondes = this._totalSecondes - this._minutes * 60;
    }, 1000);
  }
  stop() {
    clearInterval(this._timer);
  }
  reset() {
    this.stop();
    this._totalSecondes = this._minutes = this._secondes = 0;
  }

  setTimer(minutes: number, secondes: number) {
    this._totalSecondes = minutes * 60 + secondes;
    this._minutes = minutes;
    this._secondes = secondes;
  }
}

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
})

export class ResultComponent implements OnInit { // manage the results display

  currentMoreDataValue:string = "";
  currentMoreDataIndex:number = 0;
  activeIndex: number = 0;
  result: any;
  xmlName: string = "";
  xhr: XMLHttpRequest = new XMLHttpRequest();
  numberOfSteps: number = -1;
  steps: any[];
  currentStep: number = 0;
  remainingTime: Timer = new Timer();
  completionPercentage: number = 0;
  timeFinished:Date = new Date(0); // create a default date set at 0 ms.
  units = "ms"; // largest time to display for the finish time: seconds | min | hour | day | mounth
  intervalDateUpdate:any; // interval that will update current time every seconde
  currentDate:Date = new Date(); //current date used to compute the elapsed time.
  timeDiffString = ""; // Display the time elapsed since the end of the loading with units
  displayMaximizable: boolean = false;
  aborted:boolean = false;
  done:boolean = false;



  @ViewChild('timeline') timeline: Timeline | undefined;

  constructor(private service:XmlService, private route: ActivatedRoute, private data: DataService){
    this.steps = [];
  }

ngOnInit(): void {
  this.xmlName = this.data.xmlPath;
  // this.route.queryParamMap.subscribe(params => {
  //   this.xmlName = this.data.xmlPath; // params.get('xmlName')!;
  //   console.log(this.xmlName)
  // })
  this.service.getXml(this.xmlName).then(xml=>{
    this.xhr = this.service.runCydre3(xml);
    this.remainingTime.start();
    // receive every response from the api
    let tempreponse = "";
    let parent = this;
    this.xhr.addEventListener("progress", function () {
      if (this.readyState == this.LOADING) {
        let finalresponse = this.responseText.replace(tempreponse, "");
        tempreponse = this.responseText;
        // When we spam responses, they get merged. We separate them here using a \n as a delimiter
        finalresponse.split("\n").forEach(currentLine=>{
          if (currentLine=="") // We have an enpty line at the end of a chunk, as it breaks everything we don't want it
            return;
          console.log(currentLine);
          if (parent.numberOfSteps == -1) {
            let nbSteps, firstRemainingTime;
            nbSteps = currentLine.split("|")[0];
            firstRemainingTime = currentLine.split("|")[1];
            console.log(nbSteps, firstRemainingTime);
            parent.numberOfSteps = parseInt(nbSteps);
            for (let i = 0; i < parent.numberOfSteps; i++) {
              parent.steps.push({"step":i+1, "text":"Step","SuccessText":"", "icon":"pi pi-circle", "color":"#888888", "moreData":false, "typeMoreData":"", "moreDataValue":""});
            }
            parent.steps[0].icon = "pi pi-cog";
            parent.steps[0].color = "#5468FF";
            parent.steps.push({"step":parent.numberOfSteps+1, "text":"FIN", "SuccessText":"", "icon":"pi pi-circle", "color":"#888888", "moreData":false, "typeMoreData":"", "moreDataValue":""});

            parent.steps = [...parent.steps];
            console.log(parent.steps);
            parent.remainingTime.setTimer(parseInt(firstRemainingTime.split(":")[0]), parseInt(firstRemainingTime.split(":")[1]));
          } else {
            let finishedText, newCompletion, remainingTime, moreData, moreDataValue, typeMoreData;
            moreData = currentLine.split("|")[3]
            if (moreData == "true") {
              [finishedText, newCompletion, remainingTime,typeMoreData,moreDataValue] = currentLine.split("|");
            } else {
              [finishedText, newCompletion, remainingTime] = currentLine.split("|");
            }
            console.log(finishedText, newCompletion, remainingTime)
            // If the step is not complete, the successText is just ""
            if (finishedText!="") {
              parent.steps[parent.currentStep].successText = finishedText;
              parent.steps[parent.currentStep].icon = "pi pi-check";
              parent.steps[parent.currentStep].color = "#29f334";
              parent.steps[parent.currentStep].moreData = (moreData == "true");
              if (moreData == "true") {
                parent.steps[parent.currentStep].typeMoreData = typeMoreData;
                parent.steps[parent.currentStep].moreDataValue = moreDataValue;
              }

              if (parent.currentStep + 1 < parent.numberOfSteps) {
                parent.steps[parent.currentStep + 1].icon = "pi pi-cog";
                parent.steps[parent.currentStep + 1].color = "#5468FF";
              } else {
                parent.steps[parent.currentStep+1].icon = "pi pi-check";
                parent.steps[parent.currentStep+1].color = "#29f334";
              }
              parent.steps = [...parent.steps];
              parent.currentStep++;
            }
            if (newCompletion!="")
              parent.completionPercentage = parseInt(newCompletion);
            if (remainingTime!="")
              parent.remainingTime.setTimer(parseInt(remainingTime.split(":")[0]), parseInt(remainingTime.split(":")[1]));
            console.log(parent.steps);
            if (parent.currentStep == parent.numberOfSteps) { //When the last step is reached
              parent.remainingTime.stop();
              parent.done = true;
              parent.timeFinished = new Date();
              parent.updateCurrentDate();
              parent.activeIndex = -1;
            }
          }
        });

      }
    });
  });
}

updateCurrentDate() {
  this.timeDiffString = "0s";
  this.intervalDateUpdate = setInterval(() => {
    this.currentDate = new Date();
    this.timeDiffString = this.computeTimeDiff(this.timeFinished,this.currentDate);
  },1000)
}

computeTimeDiff(start:Date, end:Date):string{
  var diffString = "";
  var diff = end.getTime()-start.getTime();
  var days = Math.floor(diff / (60 * 60 * 24 * 1000));
  var hours = Math.floor(diff / (60 * 60 * 1000)) - (days * 24);
  var minutes = Math.floor(diff / (60 * 1000)) - ((days * 24 * 60) + (hours * 60));
  var seconds = Math.floor(diff / 1000) - ((days * 24 * 60 * 60) + (hours * 60 * 60) + (minutes * 60));
  if(days > 0){
    diffString = days + " days";
  }
  else if(hours > 0){
    diffString = hours + " hours";
  }
  else if(minutes){
    diffString = minutes + " min";
  }
  else{
    diffString = seconds + " s";
  }
  return diffString;
}

showMaximizableDialog(moreDataValue:string,step:number){
  this.currentMoreDataIndex = step;
  this.currentMoreDataValue = moreDataValue;
  this.displayMaximizable = true;
}

abort(){
  this.xhr.abort();
  this.aborted = true;
  this.remainingTime.reset();
  this.activeIndex = -1;
  this.timeFinished = new Date();
  this.updateCurrentDate();
}

ngOnEnd(){
  clearInterval(this.intervalDateUpdate);
}

}

