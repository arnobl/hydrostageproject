import { Component, AfterViewInit, EventEmitter, Output } from '@angular/core';
import * as L from 'leaflet';
import MeasurementPoint from 'src/app/out/model/MeasurementPoint';
import { DataService } from 'src/app/out/service/data.service';
import { AutoCompleteCompleteEvent } from 'primeng/autocomplete';



@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})

export class MapComponent implements AfterViewInit{



  selectedItem : string[] = [];
  locations: string[] = [];
  filteredLocations: string[]= [];

  map!: L.Map;
  markers: L.CircleMarker[] = [];
  points : Array<MeasurementPoint> = [];
  pointsClicks : Map<MeasurementPoint, number> = new Map<MeasurementPoint, number>([]);

  @Output() pointsClicksChange: EventEmitter<Map<MeasurementPoint, number>> = new EventEmitter<Map<MeasurementPoint, number>>();

  //basemaps :
  //classic (openstreetmap ou positron)
  //satellite
  //noir et blanc
  //relief
  basemaps = {
    "OpenStreetMap" : L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }),
    "Positron" : L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }),
    "Relief Couleur" : L.tileLayer('https://tile.geobretagne.fr/geopf/service?layer=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2&style=&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=png&TileMatrix={z}&TileCol={x}&TileRow={y}', {
      attribution: '&copy; <a href="https://www.ign.fr/">IGN</a>'
    }),
    "Relief Gris" : L.tileLayer('https://tile.geobretagne.fr/osm/service?layer=osm:grey&style=&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=png&TileMatrix={z}&TileCol={x}&TileRow={y}', {
      attribution: '&copy; Données <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> mises en forme par <a href="https://www.osm.datagrandest.fr/">Data Grand Est</a>'
    }),
    "Dark Matter":  L.tileLayer('http://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png',{
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }),
    "Photo Satellite": L.tileLayer('https://tile.geobretagne.fr/photo/service?layer=HR.ORTHOIMAGERY.ORTHOPHOTO&style=&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=png&TileMatrix={z}&TileCol={x}&TileRow={y}', {
      attribution: '&copy; <a href="https://www.ign.fr/">BD Ortho IGN</a> '
    })
  };

  //geoJsonWfs = L.tileLayer.GeoJSON.WFS('https://ows.region-bretagne.fr/geoserver/rb/wfs?', {

  overlaymaps = {
    "Bassins versants" : L.tileLayer.wms('https://ows.region-bretagne.fr/geoserver/rb/wms?', {
      layers : 'bassin_versant',
      attribution : '&copy; <a href="https://www.bretagne.bzh/">Région Bretagne</a>',
      transparent : true,
      format : "image/png"
    })
  };

  markerOptions = {
    baseSize : 6,
    hoverSize : 9,
    baseColor : '#3C91E6',
    clickedColor : '#8FBE23'
  }

  constructor(private dataService : DataService){
  }

  initLocations(){
    this.points.forEach((measurePoint: MeasurementPoint) => {
      this.locations.push(this.decode(measurePoint.name));
    });
  }

  selectPoint(event: any) {
    let index = this.locations.indexOf(event);
    let marker = this.markers[index];
    let point = this.points[index];
    marker.setStyle({color : this.markerOptions.clickedColor});
    this.pointsClicks.set(point,1);
    this.pointsClicksChange.emit(new Map(this.pointsClicks));
  }

  unselectPoint(event: any) {
    let index = this.locations.indexOf(event);
    let marker = this.markers[index];
    let point = this.points[index];
    marker.setStyle({color : this.markerOptions.baseColor});
    this.pointsClicks.set(point,0);
    this.pointsClicksChange.emit(new Map(this.pointsClicks));
  }

  initMap() {
    this.map = L.map('map', {
      center : [48.202047, -2.932644],
      zoomControl : false,
      zoom: 8,
      minZoom : 8,
      maxZoom : 18,
      layers : [this.basemaps['Positron']]
    });

    let zoomControl = L.control.zoom({
      position:'topright'
    }).addTo(this.map);

    let layerControl = L.control.layers(this.basemaps, this.overlaymaps, {position : 'bottomright'}).addTo(this.map);

    let scaleControl = L.control.scale({maxWidth : 100, imperial : false}).addTo(this.map);
  }

  initMarkers(){
    let p = new Promise<void>((resolve) => {
      this.dataService.getMesurementCoordinates().then((d: any) => {
        d.forEach((measurePoint: MeasurementPoint | undefined) => {
          if (measurePoint !== undefined){

            this.points.push(measurePoint);
            let lat = Number(measurePoint.y_outlet);
            let long = Number(measurePoint.x_outlet);

            let jschardet = require("jschardet");

            const marker = this.generateMarker(measurePoint, lat, long);

            marker.addTo(this.map).bindPopup(`<b>${this.decode(measurePoint.name)}</b>`);
            this.markers.push(marker);
          }
        });
        this.initLocations();
        resolve();
      });
    });
  }


  generateMarker(index: MeasurementPoint, xcoord : number, ycoord : number) {
    let baseSize = this.markerOptions.baseSize;
    let hoverSize = this.markerOptions.hoverSize;
    let m =  new L.CircleMarker([xcoord, ycoord], {radius: baseSize, color: this.markerOptions.baseColor})
        .on('click', (event) => {
          this.markerClicked(event, index, m);})
        .on('mouseover', function(e) {
          m.setRadius(hoverSize);//marker object is overwritten in the for loop each time
          m.openPopup();
        })
        .on('mouseout', function(e) {
          m.setRadius(baseSize);//marker object is overwritten in the for loop each time
          m.closePopup();
        });
    this.pointsClicks.set(index, 0);
    return m;
  }

  onMapReady($event: L.Map) {
    this.map = $event;
    this.initMarkers();
  }

  filterPoints(event: AutoCompleteCompleteEvent) {
    let filtered: string[] = [];
    for(let i = 0; i < this.locations.length; i++) {
        let location = this.locations[i];
        //ici pour modifier l'algorithme de recommendation stp
        // if(location.toLowerCase().indexOf(event.query.toLowerCase()) == 0 && !this.selectedItem.includes(location)) {
        //     filtered.push(location);
        // }
        if((location.toLowerCase()).includes(event.query.toLowerCase()) && !this.selectedItem.includes(location)) {
          filtered.push(location);
      }
    }
    this.filteredLocations = filtered;
  }

  //-----------------------****INTERACTION OPTION ****-----------------------

  mapClicked($event: any) {
    console.log($event.latlng.lat, $event.latlng.lng);
  }

  markerClicked($event: any, index: MeasurementPoint, marker: L.CircleMarker) {
    let numClicks = this.pointsClicks.get(index);
    if (numClicks !== undefined && numClicks%2 == 0){
      marker.setStyle({color : this.markerOptions.clickedColor});
      this.pointsClicks.set(index, ++numClicks);
      this.pointsClicksChange.emit(new Map(this.pointsClicks));

      let newSelectedItem = this.selectedItem.filter(item => true);
      newSelectedItem.push(this.decode(index.name));
      this.selectedItem= newSelectedItem;
    }
    else if (numClicks !== undefined && numClicks%2 !== 0){
      marker.setStyle({color : this.markerOptions.baseColor});
      this.pointsClicks.set(index, ++numClicks);
      this.pointsClicksChange.emit(new Map(this.pointsClicks));
      this.selectedItem = this.selectedItem.filter(item => item !== this.decode(index.name));
    }
  }

  ngAfterViewInit() {
    this.initMap();
    this.initMarkers();
  }

  //-----------------------****GOOD PRESENTATION****-----------------------
  decode(labelToDecode : string){
    try {
      return decodeURIComponent(escape(labelToDecode));
    }catch(e){
      return labelToDecode;
    }
  }

}
