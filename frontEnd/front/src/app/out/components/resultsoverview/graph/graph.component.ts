import { Component } from '@angular/core';
import type MeasurementPoint from 'src/app/out/model/MeasurementPoint';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent {

  measurePoints : MeasurementPoint[]= []
  shouldDisplayChart : boolean = false;


  onPointsClicksChange(pointsClicks: Map<MeasurementPoint, number>) {
    let local : MeasurementPoint[]= [];
    for (let [key, value] of pointsClicks) {
      if(value%2 !=  0){
        local.push(key);
      }
    }
    this.measurePoints = local
    this.shouldDisplayChart = this.measurePoints.length > 0;
  }

}
