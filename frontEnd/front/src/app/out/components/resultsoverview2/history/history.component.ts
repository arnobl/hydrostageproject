import { Component } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import {type SimulationComplete, type Filter, FilterOperators} from 'src/app/out/model/history';
import {HistoryService} from 'src/app/out/service/history.service';
import {parseString} from 'xml2js';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  providers: [ConfirmationService],
})
export class HistoryComponent {
  sidebarVisible: boolean = false;
  simulations: SimulationComplete[] = [];
  sortSelected: string = 'date';
  properties: string[] = ['date'];
  filters: Filter[] = [];
  globalCheck: boolean = false;
  currentSimulationInfo?: any;
  infoVisible: boolean= false;



  constructor(private confirmationService: ConfirmationService, private historyService: HistoryService) {}

ngOnInit(): void {
    this.updateSimulations();
    this.updateProperties();
}

  updateProperties() {
    this.historyService.getProperties().then(
      response => {
        this.properties = response;
      },
      error => {
        console.error('Error:', error);
      }
    );
  }
  updateSimulations() {
    this.historyService.getHistory(this.filters, this.sortSelected).then(
      response => {
        this.simulations = response.map(simulation => {
          return {
            ...simulation,
            checked: false,
            rename: false,
            renameValue: simulation.name
          }
        });
        console.log('hola')
      },
      error => {
        console.error('Error:', error);
      }
    );
  }
  addFilter() {
      this.filters.push({field: '', value: '', operator: '='});
  }


  removeFilter(filter: Filter) {
    this.filters = this.filters.filter(f => f !== filter);
    this.updateSimulations();
  }

  reset() {
    this.filters = [];
    this.sidebarVisible = false;
    this.updateSimulations();
  }

  deleteSimulation(simu: SimulationComplete) {
    this.confirmationService.confirm({
      message: 'Êtes-vous sûr de vouloir supprimer cette simulation ? Cette action est irréversible.\nSimuation : ' + simu.name,
      header: 'Suppression de simulation',
      acceptLabel: 'Supprimer',
      rejectLabel: 'Annuler',
      icon: 'pi pi-exclamation-triangle',
      rejectButtonStyleClass: 'p-button-secondary',
      acceptButtonStyleClass: 'p-button-danger',
      accept: () => {
        this.historyService.deleteSimulation(simu).then(
          _ => {
            this.updateSimulations();
          }
        );
      }
    });

  }

  stringifySimu(simu: any) : string[] {
    let simuString = [];
    for (let key in simu) {
      if (key !== 'checked' && key !== 'rename' && key !== 'renameValue' && key !== 'favorite' && key !== 'properties') {
        simuString.push(key + ' : ' + simu[key]);
      }
      if(key === 'properties'){
        for(let i = 0; i < simu[key].length; i++){
          if(!Object.keys(simu).includes(simu[key][i].name)){
            simuString.push(simu[key][i].name + ' : ' + simu[key][i].value);
          }
        }
      }
    }
    return simuString;
  }

  updateCurrentSimulation(simu: SimulationComplete) {
    console.log('test')
    this.historyService.getSimulationXml(simu).then(
      response => {
        parseString(response, { explicitArray: false, mergeAttrs: true },(err, result) => {
          if(err) {
            console.error('Error:', err);
          }
          this.currentSimulationInfo = result;
          console.log(this.currentSimulationInfo)
        });
      }
    );
  }

  selectAll() {
    for (let i = 0; i < this.simulations.length; i++) {
        this.simulations[i]['checked'] = !this.globalCheck;
    }
  }

  validate() {
    let selectedSimulations = [];
    for (let i = 0; i < this.simulations.length; i++) {
      if (this.simulations[i]['checked']) {
        selectedSimulations.push(this.simulations[i]);
      }
    }
    // TODO : plot selected simulations
    this.sidebarVisible = false;
  }

  getIcon(fav: boolean) {
    return fav ? "pi pi-star-fill" : "pi pi-star";
  }

  showDialog(){
    this.infoVisible = true;
  }

  setName(simu: SimulationComplete) {
    simu.rename = !simu.rename;
    this.historyService.renameSimulation(simu).then(
      _ => {
        simu.name = simu.renameValue;
      }
    );
  }

  protected readonly FilterOperators = FilterOperators;

  setFavorite(simu: SimulationComplete) {
    this.historyService.setSimulationFavorite(simu, !simu.favorite).then(
      response => {
        simu.favorite = response;
        this.updateSimulations();
      }
    );
  }
  onDropdownShow() {
    const panel : HTMLElement = document.getElementsByClassName('p-dropdown-panel')[0] as HTMLElement
    if(panel != null) {
      panel.style.width = '300px'
    }

  }
  protected readonly console = console;
  protected readonly alert = alert;
}
