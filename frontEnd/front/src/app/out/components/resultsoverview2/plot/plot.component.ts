import { Component } from '@angular/core';

@Component({
  selector: 'app-plot',
  templateUrl: './plot.component.html',
  styleUrls: ['./plot.component.scss'],
})
export class PlotComponent {
  config = {
    responsive: true,
  }
  public graph = {
    data: [
      { x: [1, 2, 3], y: [2, 6, 3], type: 'scatter', mode: 'lines+points', marker: {color: 'red'} },
      { x: [1, 2, 3], y: [2, 5, 3], type: 'scatter', mode: 'lines+points', marker: {color: 'blue'} },
      { x: [1, 2, 3], y: [2, 4, 3], type: 'scatter', mode: 'lines+points', marker: {color: 'green'} },
    ],
    layout: {title: 'Plot title', autosize:true}
  };
}



