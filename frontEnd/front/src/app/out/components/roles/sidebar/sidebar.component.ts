import { Component } from '@angular/core';
import { DataService } from 'src/app/out/service/data.service';
import { XmlService } from 'src/app/out/service/xml.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {

  protected xmlNames: Array<string>;
  protected filteredxmlNames:Array<string>;
  //protected currentName: string | undefined;

  public constructor(private requestService: XmlService, private data: DataService){
    this.xmlNames = [];
    this.filteredxmlNames = [];

  }


  public ngAfterViewInit(): void {
     // we request all xml names
    this.requestService.getXmlNames()
    .then(names => {
      this.xmlNames = names;
      this.filteredxmlNames=names;
    })
    .catch(err => {
      console.log(err);
    })
  }

  protected async sendXmlName(name:string){
    //this.currentName=name;
    this.data.xmlPath = name;

    await this.data.requestXml();
      this.data.getJSONData() // load the xml parsed into JSON in parameters
      .then(response=>{}).catch(error=>{});

  }

  get currentName() {
    return this.data.xmlPath;
  }

  keyPress(text: string) {
    if (!text || text.length<1) {
      this.filteredxmlNames = this.xmlNames;
      return;
    }
    this.filteredxmlNames = this.xmlNames.filter(
      xmlname => xmlname?.toLowerCase().includes(text.toLowerCase())
    );

    }

}
