import { Component } from '@angular/core';
import {DataService} from 'src/app/out/service/data.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent {

  constructor(private service: DataService){
    //this.parameters = {name:[""]};
  }

  get userRole(){
    return this.service.userRole;
  }
}
