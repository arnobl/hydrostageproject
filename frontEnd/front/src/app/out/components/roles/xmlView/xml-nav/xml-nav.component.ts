import {ChangeDetectionStrategy, Component, Input, SimpleChanges} from '@angular/core';
import type {Parameter, ParametersGroup} from 'src/app/out/model/parameters-group';

@Component({
  selector: 'app-xml-nav',
  templateUrl: './xml-nav.component.html',
  styleUrls: ['./xml-nav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class XmlNavComponent {


  // @Input() in order to give the variable a value from the html
  @Input() parameters: ParametersGroup;


  current_path: Array<string>;
  isLoading: boolean = true;
  currentParamGrp: ParametersGroup;

  constructor() {
    this.parameters = {name: [""]};
    this.current_path = new Array<string>();
    this.currentParamGrp = this.parameters;
  }

  ngOnInit(): void {
    this.currentParamGrp = this.parameters;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['parameters']) {
      this.isLoading = false;
      this.current_path = new Array<string>();
      this.currentParamGrp = this.parameters;
    }
  }

  isParameter(element: any): element is Parameter {
    return element && element.name !== undefined;
  }

  isParameterGrp(element: any): element is ParametersGroup {
    return element && element.name !== undefined;
  }

  existParamGroup(element: any): boolean {
    return (element as any).ParametersGroup !== undefined;
  }

  existParam(element: any): boolean {
    return (element as any).Parameter !== undefined;
  }

  navigateTo(path: string) {

    if(path === ""){
      this.current_path = new Array<string>();
      this.currentParamGrp = this.getCurrentParamGroup();
      return;
    }

    const index = this.current_path.indexOf(path);

    if (index === -1) {
      this.current_path.push(path);
      console.log(this.current_path)
    } else {
      this.current_path.splice(index + 1);
      console.log(this.current_path)
    }

    this.currentParamGrp = this.getCurrentParamGroup();
  }

  getCurrentParamGroup(){
    var currentParamGrp = this.parameters;

    for(let i= 0; i<this.current_path.length; i++){

      if(currentParamGrp.ParametersGroup === undefined)
        throw new Error(`ParameterGroup '${this.current_path[i]}' not found `);

      currentParamGrp = this.findParamGroupByName(currentParamGrp.ParametersGroup, this.current_path[i]);
    }
    return currentParamGrp;
  }

  private findParamGroupByName(params: ParametersGroup[], path: string) {
    for(let i= 0; i < params.length; i++){
      if(params[i].name[0] === path){
        return params[i];
      }
    }
    throw new Error(`ParameterGroup '${path}' not found `);
  }

  public formatSegment(segment: string | undefined): string {
    if(segment === undefined) return "";
    if (!segment) return segment;
    let formattedSegment = segment.replace(/_/g, ' ');
    return formattedSegment.charAt(0).toUpperCase() + formattedSegment.slice(1);
  }

  protected readonly length = length;
}

