import { Injectable } from '@angular/core';
import { Parameter, ParametersGroup } from '../model/parameters-group';
import { Parser,Builder} from 'xml2js';
import { XmlService } from './xml.service';
import { JsonService } from './json.service';
import MeasurementPoint from '../model/MeasurementPoint';
import Measure from '../model/Measure';
import type {Rules} from '../model/rules';
import * as jsonPath from 'jsonpath-plus';
import {JSONPath} from "jsonpath-plus";
import _roleParameters from '../../../assets/json/testconfig.json';

interface RoleParameters {
  [file: string]: {
    [role: string]: string[];
  };
}

@Injectable({
  providedIn: 'root'
})
export class DataService { //service used to load the received xml and to convert it into json and vice versa

  private _userRole: string ="Scientist";

  private xmlData: string = ""; // variable to load the xml

  public xmlPath:string = ""; // path for the request of an xml

  public _parameters: ParametersGroup={name:[""]};

  public exclusionList: RoleParameters = _roleParameters;

  private xmlRules: string = "";

  constructor(private requestService: XmlService, private jsonService : JsonService) {}


  get userRole(){
    return this._userRole;
  }

  set userRole(role:string){
    this._userRole=role;
  }

  get parameters():ParametersGroup{
    return this._parameters;
  }

  get available_roles():string[]{
    return Array.from(
      new Set(
        Object.values(this.exclusionList)
          .flatMap((roles) => Object.keys(roles))
      )
    );
  }
  getMesurementCoordinates(): Promise<Array<MeasurementPoint>>{ // request the list of measurement points
    return this.jsonService.getCoordinates();
  }

  getMesurementData(id : string): Promise<Array<Measure>>{ // request all the data for the given point
    return this.jsonService.getData(id);
  }

  getMesurementDataByDate(id : string, start : Date, end : Date): Promise<Array<Measure>>{ // request all the data for the given point and the given dates
    return this.jsonService.getData(id).then(data => {
      return data.filter(m => {
        return m.t >= start && m.t <= end;
      });
    });
  }

  jsonDataToCsv(jsonData: Array<Measure>): string { // convert the json to csv
    let csvData = "t,Q\n";
    jsonData.forEach(m => {
      csvData += `${m.t.toISOString()},${m.Q}\n`;
    });
    return csvData;
  }

  requestXml() {
    this.requestService.getXml(this.xmlPath)
      .then(xml => {
        this.xmlData = xml;
        // console.log(this.xmlData);
      })
      .catch(err => {
        console.log(err)
      })

    this.requestService.getXmlRules(this.xmlPath)
      .then(xml => {
        this.xmlRules = xml;
        // console.log(this.xmlRules);
      })
      .catch(err => {
        console.log(err)
      })
  }


  async parseXml(xmlString: string) {
    const parser = new Parser({
      "mergeAttrs": true,
    });
    return await new Promise((resolve, reject) => parser.parseString(xmlString, (err: any, jsonData: any) => {
      if (err) {
        reject(err);
      }
      resolve(jsonData);
    }));
  }

  public async getJSONData(): Promise<ParametersGroup> {
    return this.parseXml(this.xmlData)
    .then((res: any) => {
      console.log("calling getJSONData");
      this._parameters = this.filterParametersGroup(res.ParametersGroup as ParametersGroup) as ParametersGroup;
      return this._parameters;
    }).catch(error=>{return {name:[""]} as ParametersGroup}) //nécessaire ? utile ?;
    // return this.parseXml(this.xmlData)
    //   .then((res: any) => {
    //     // console.log(res.ParametersGroup);
    //     return res.ParametersGroup as ParametersGroup;
    //   })
    // // .then(group => {
    // // 	this.setParent(group);
    // // 	return group;
    // // });
  }

  private filterParametersGroup(parametersGroup: ParametersGroup): ParametersGroup |null {

    if (!parametersGroup || !parametersGroup.name) {
      return parametersGroup;
    }

    const groupName = parametersGroup.name[0];
    if (this.exclusionList[this.xmlPath][this.userRole] && this.exclusionList[this.xmlPath][this.userRole].includes(groupName)) {
      // If the current group is in the exclusion list, return null to filter it out
       return null;
    }
    if (parametersGroup.ParametersGroup instanceof Array) {
      // Recursively filter nested ParametersGroups
      const filteredGroups= parametersGroup.ParametersGroup
        .map((group: ParametersGroup) => this.filterParametersGroup(group))
        .filter((group: ParametersGroup | null) => group !== null) as ParametersGroup[];

      // If there are remaining groups after filtering, update the original structure
      if (filteredGroups.length > 0) {
        parametersGroup.ParametersGroup = filteredGroups;
      } else {
        // If all nested groups were filtered out, set ParametersGroup to null
        parametersGroup.ParametersGroup = [];
      }
    }

    return parametersGroup;
  }


  public async getJSONRules(): Promise<Rules> {
    return this.parseXml(this.xmlRules)
      .then((res: any) => {
        // console.log(res.rules);
        return res.rules as Rules;
      })
  }

//   private setParent(param: ParametersGroup | Parameter, parent?: ParametersGroup): void {
// 	param.parent = parent;
// 	if(!("type" in param)) {
// 		param.ParametersGroup?.forEach(g => {
// 			this.setParent(g, param);
// 		});
// 		param.parameters?.forEach(sub => {
// 			this.setParent(sub, param);
// 		});
// 	}
//   }

  jsonToXml(json:any):string{ // convert the json to xml
	const options ={
		attrkey:"$",
		rootName: "ParametersGroup",
		headless: true,
	}
	const builder = new Builder(options);
	const xmlData = builder.buildObject(json);
	console.log(xmlData);
	return xmlData;
  }
}
