import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, lastValueFrom } from 'rxjs';
import MeasurementPoint from '../model/MeasurementPoint';
import Measure from '../model/Measure';

@Injectable({
  providedIn: 'root'
})
export class JsonService {

  constructor(private http: HttpClient){}

	getCoordinates(): Promise<Array<MeasurementPoint>>{ // request the list of measurement points
		return lastValueFrom(this.http.get<Array<MeasurementPoint>>("osur/getCoordinates"));
	}
  getData(id : string): Promise<Array<Measure>>{ // request all the data for the given point
    const url = `osur/getJson?ID=${id}`;
    return lastValueFrom(this.http.get<Array<Measure>>(url));
	}
}
