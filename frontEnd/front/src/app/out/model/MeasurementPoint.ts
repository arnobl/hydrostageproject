class MeasurementPoint {
  Area: string;
  ID: string;
  name: string;
  x_outlet: string;
  y_outlet: string;

  constructor(name: string, ID: string, x_outlet: string, y_outlet: string, Area: string) {
    
    let pointName;
    try{
        // If the string is UTF-8, this will work and not throw an error.
      pointName=decodeURIComponent(escape(name));
    }catch(e){
        // If it isn't, an error will be thrown, and we can assume that we have an ISO string.
        pointName=name;
    }
    this.name = pointName;
    this.ID = ID;
    this.x_outlet = x_outlet;
    this.y_outlet = y_outlet;
    this.Area = Area;
  }
}

export default MeasurementPoint;
