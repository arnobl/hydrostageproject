export interface Rules {
  constraint: Array<Constraint>
}

export interface Constraint {
  condition: Array<Condition>
  action: Array<Action>
}

export interface Condition {
  parametersGroup: string
  parameterName: string
  logicalOperator: string
  value: string
}

export interface Action {
  parametersGroup: string
  parameterName: string
  logicalOperator: string
}
