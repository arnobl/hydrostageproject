
type FilterOperatorsType = '='| '!='| '>'| '<'| '>='| '<=';
export const FilterOperators: FilterOperatorsType[] = ['=', '!=', '>', '<', '>=', '<='];

export interface Filter {
  field: string;
  value: string;
  operator: FilterOperatorsType;
}

interface Property {
  name: string;
  value: string;
}

export interface Simulation {
  name: string;
  date: string;
  favorite: boolean;
  properties: Property[];
}


export interface SimulationComplete extends Simulation {
  checked: boolean;
  rename: boolean;
  renameValue: string;
}
