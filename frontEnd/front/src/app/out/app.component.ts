// node v18.16.0
// npm v6.14.18
// primeng v16.0.0



import { Component,HostListener} from '@angular/core';
import { MessageService } from 'primeng/api';
import JSZip from 'jszip';

interface ScrollData{
  time: number;
  position: number;
}

interface CustomFile{
  name: string;
  content: Blob;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [MessageService]
})
export class AppComponent{
  title = 'front';

  // Variable pour savoir si on est en train d'enregistrer ou non
  recording = false;

  // Variable pour connaitre le temps écoulé depuis le début de l'enregistrement
  private chronometer: any;
  private elapsedTime = 0;

  private interval = 100; // 100 ms

  private scrollData: ScrollData[] = [];
  private clickCount = 0;

  constructor(private messageService: MessageService) { }


  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    this.handleOnKeyDown(event);
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event: Event) {
    this.handleOnScroll();
  }

  @HostListener('click', ['$event'])
  onClick(event: MouseEvent) {
    this.handleOnClick(event);
  }


  handleOnClick(event: MouseEvent) {
    if (this.recording) {
      this.clickCount++;
    }
  }


  //Scroll part

  handleOnScroll() {
    if(this.recording){
      const currentScrollPosition = window.scrollY;
      this.scrollData.push({
        time: this.elapsedTime,
        position: -currentScrollPosition
      });
    }
  }

  // Chronomètre part
  handleOnKeyDown(event: KeyboardEvent) {
    if (event.key === '*') {
      this.recording = !this.recording;
      if(this.recording){
        console.log("recording");
        this.messageService.add({ severity: 'success', summary: 'Success', detail: "Démarrage de l'enregistrement" });
      }else{
        console.log("not recording");
        this.messageService.add({ severity: 'success', summary: 'Success', detail: "Fin de l'enregistrementé" });

      }
      this.handleOnScroll();
      if (this.chronometer) {
        clearInterval(this.chronometer);
        this.chronometer = null;
        this.saveData();
      } else {
        this.chronometer = setInterval(() => {
          this.elapsedTime += this.interval ; // Incrémenter de 100 ms
        }, this.interval );
      }
    }
  }

  // Fonction pour formater le temps au format mm:ss
  formatTime(ms: number): string {
    const milliSeconds = Math.floor(ms % 1000);
    const seconds = Math.floor((ms-milliSeconds) / 1000) % 60;
    const minutes = Math.floor((ms-milliSeconds-seconds*1000) / 60000);
    return `${this.padTime(minutes)}m:${this.padTime(seconds)}s:${this.padTime(milliSeconds)}ms`;
  }

  // Fonction pour ajouter un zéro devant les chiffres < 10
  padTime(value: number): string {
    return value < 10 ? `0${value}` : `${value}`;
  }

  saveData(){
    this.scrollData = this.normalizeScrollData(this.scrollData);
    let files : CustomFile[] = []
    let csvReportFile : CustomFile = {
      name: `rapport.txt`,
      content: this.generateReportFile(this.clickCount)
    }
    files.push(csvReportFile);
    let csvScrollFile : CustomFile = {
      name: `scrollData.csv`,
      content: this.generateCsv(this.scrollData)
    }
    files.push(csvScrollFile);
    this.generateZipBlob(files).then((blob: Blob) => {
      this.makeDownload(blob, `rapport.zip`);
      this.elapsedTime = 0;
      this.scrollData = [];
    })

  }

  generateReportFile(clickCount: number): Blob {
    const content = `Nombre de clics : ${clickCount}\nTemps total : ${this.formatTime(this.elapsedTime)}`;
    return new Blob([content], { type: 'text/plain' });
  }

   generateCsv( data : any[] ): Blob {
    if (data.length === 0) {
      throw new Error('La liste de données est vide.');
    }

    const headers = Object.keys(data[0]);

    let csvData = headers.join(',') + '\n';

    data.forEach((obj) => {
      const values = headers.map((header) => obj[header]);
      csvData += values.join(',') + '\n';
    });

    return new Blob([csvData], { type: 'text/csv' });
  }

  makeDownload(file: Blob, name: string) {
    let link = document.createElement("a");
    link.href = URL.createObjectURL(file);
    link.download = name;
    link.click();
  }

  generateZipBlob(fileList: CustomFile[]): Promise<Blob> {
    const zip = new JSZip();

    // Ajouter chaque blob au fichier ZIP
    fileList.forEach(file => {
      zip.file(file.name, file.content);
    });

    // Générer le fichier ZIP
    return zip.generateAsync({ type: 'blob' });
  }

  normalizeScrollData(inputData: ScrollData[]): ScrollData[] {
    const normalizedData: ScrollData[] = [];

    const timeToOccurences : Map<number,ScrollData[]> = new Map<number,ScrollData[]>();

    inputData.forEach((data) => {
      const time = data.time;
      if(timeToOccurences.has(time)){
        timeToOccurences.get(time)?.push(data)
      }
      else{
        timeToOccurences.set(time,[data]);
      }
    });

    timeToOccurences.forEach((value,key) => {
      value.forEach((data,index) => {
        normalizedData.push({
          time: key+ Math.floor(this.interval/value.length*index),
          position: data.position
        })
      });
    });

    return normalizedData;
  }
}
