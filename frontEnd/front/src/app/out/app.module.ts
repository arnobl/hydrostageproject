import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { TreeTableModule } from 'primeng/treetable';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { InteractoModule } from 'interacto-angular';
import {DataViewModule} from "primeng/dataview";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {OrderListModule} from "primeng/orderlist";
import {ProgressBarModule} from 'primeng/progressbar';
import {SplitterModule} from 'primeng/splitter';
import {DividerModule} from 'primeng/divider';
import {TimelineModule} from 'primeng/timeline';
import {CardModule} from 'primeng/card';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {CalendarModule} from 'primeng/calendar';
import {ChartModule} from 'primeng/chart';
import {InputSwitchModule} from 'primeng/inputswitch';
import {MenuModule} from 'primeng/menu';
import {ColorPickerModule} from 'primeng/colorpicker';
import {SelectButtonModule} from 'primeng/selectbutton';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {ToastModule} from 'primeng/toast';
import * as PlotlyJS from 'plotly.js-dist-min';
import { PlotlyModule } from 'angular-plotly.js';
import {AccordionModule} from "primeng/accordion";
import {PanelModule} from "primeng/panel";
import {ScrollPanelModule} from "primeng/scrollpanel";
import {DialogModule} from "primeng/dialog";
import {MenuOverviewComponent} from './out/components/resultsoverview/menuoverview/menuoverview.component';
import {HeaderComponent} from './out/components/xmlView/header/header.component';
import {HeaderRoleComponent} from './out/components/roles/xmlView/header/headerrole.component';
import {BodyComponent} from './out/components/xmlView/body/body.component';
import {MenuComponent} from './out/components/menu/menu.component';
import {XmlComponent} from './out/components/xmlView/xml/xml.component';
import {GraphComponent} from './out/components/resultsoverview/graph/graph.component';
import {MapComponent} from './out/components/resultsoverview/map/map.component';
import {ChartComponent} from './out/components/resultsoverview/chart/chart.component';
import {ResultComponent} from './out/components/result/result.component';
import {HistoryComponent} from './out/components/resultsoverview2/history/history.component';
import {PlotComponent} from './out/components/resultsoverview2/plot/plot.component';
import {Result2Component} from './out/components/resultsoverview2/result/result2.component';
import {XmlNavComponent} from './out/components/roles/xmlView/xml-nav/xml-nav.component';
import {TopbarComponent} from './out/components/roles/topbar/topbar.component';
import {SidebarComponent} from './out/components/roles/sidebar/sidebar.component';
import {MaincontentRoleComponent} from './out/components/roles/xmlView/maincontent/maincontentrole.component';
import {MainPageComponent} from './out/components/roles/main-page/main-page.component';
import {SidebarModule} from 'primeng/sidebar';
import {MatIconModule} from '@angular/material/icon';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {InputTextModule} from 'primeng/inputtext';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {MenuRolesComponent} from './out/components/roles/menuroles/menuroles.component';
import {BodyRoleComponent} from './out/components/roles/xmlView/body/bodyrole.component';
import {XmlRoleComponent} from './out/components/roles/xmlView/xml/xmlrole.component';

PlotlyModule.plotlyjs = PlotlyJS;

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    MenuComponent,
    MenuOverviewComponent,
    Result2Component,
    HistoryComponent,
    PlotComponent,
    GraphComponent,
    MapComponent,
    ChartComponent,
    XmlComponent,
    ResultComponent,
    XmlNavComponent,
    TopbarComponent,
    SidebarComponent,
    MainPageComponent,
    MenuRolesComponent,
    HeaderRoleComponent,
    MaincontentRoleComponent,
    BodyRoleComponent,
    XmlRoleComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    TableModule,
    ButtonModule,
    TreeTableModule,
    InteractoModule,
    SidebarModule,
    MatIconModule,
    DropdownModule,
    InputTextareaModule,
    InputTextModule,
    ConfirmDialogModule,
    OverlayPanelModule,
    PlotlyModule,
    AccordionModule,
    PanelModule,
    ScrollPanelModule,
    DialogModule,
    ProgressBarModule,
    SplitterModule,
    DividerModule,
    TimelineModule,
    CardModule,
    LeafletModule,
    CalendarModule,
    ChartModule,
    InputSwitchModule,
    MenuModule,
    ColorPickerModule,
    SelectButtonModule,
    AutoCompleteModule,
    ToastModule,
    DataViewModule,
    OrderListModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
