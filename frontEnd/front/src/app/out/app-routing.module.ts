import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './out/components/menu/menu.component';
import { XmlComponent } from './out/components/xmlView/xml/xml.component';
import { ResultComponent } from './out/components/result/result.component';
import {MenuOverviewComponent} from './out/components/resultsoverview/menuoverview/menuoverview.component';
import {Result2Component} from './out/components/resultsoverview2/result/result2.component';
import {MenuRolesComponent} from './out/components/roles/menuroles/menuroles.component';
import {MainPageComponent} from './out/components/roles/main-page/main-page.component';
import {XmlRoleComponent} from './out/components/roles/xmlView/xml/xmlrole.component';

const routes: Routes = [ //indicate which component to load depending on the path
{
  path:"roles",
  component: MainPageComponent
},
{
  path:"xmlViewRole",
  component: XmlRoleComponent
},
{
  path:"overview",
  component: MenuOverviewComponent
},
{
  path:"overview2",
  component: Result2Component
},
  {
    path:"menu",
    component: MenuComponent
  },

  {
    path:"xmlView",
    component: XmlComponent
  },

  {
    path:"result",
    component:ResultComponent
  },

  {
    path:"**",
    redirectTo:"menu"
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
