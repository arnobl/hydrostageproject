import argparse
import zipfile
import pandas as pd
import matplotlib.pyplot as plt
from io import BytesIO

def create_and_save_plot(csv_data):
    # Lire le fichier CSV
    df = pd.read_csv(BytesIO(csv_data))

    # Créer le graphique
    plt.plot(df['time'], df['position'])
    plt.xlabel('Temps')
    plt.ylabel('Position')
    plt.legend(['Position'])
    plt.title('Graphique Position (scroll) en fonction du Temps')

    # Enregistrer le graphique dans un objet BytesIO
    img_buffer = BytesIO()
    plt.savefig(img_buffer, format='png')
    img_buffer.seek(0)

    return img_buffer.getvalue()

def main(zip_path):
    # Charger le fichier zip
    with zipfile.ZipFile(zip_path, 'a') as archive:
        # Trouver le nom du fichier CSV dans le zip
        csv_filename = next(name for name in archive.namelist() if 'scrollData.csv' in name)

        # Lire le contenu du fichier CSV
        with archive.open(csv_filename) as csv_file:
            csv_data = csv_file.read()

        # Créer et sauvegarder le graphique
        img_data = create_and_save_plot(csv_data)

        # Enregistrer le graphique dans le fichier zip
        archive.writestr('scrollData.png', img_data)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process a zip file containing scrollData.csv and create a plot.")
    parser.add_argument("zip_path", help="Path to the zip file to be processed")

    args = parser.parse_args()
    main(args.zip_path)
