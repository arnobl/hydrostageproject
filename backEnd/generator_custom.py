import csv
import random
from datetime import datetime, timedelta

# Fonction pour générer des données de débit d'un fleuve avec cohérence entre les jours
def generate_river_flow_data(start_date, end_date, min_value, max_value):
    current_date = start_date
    data = [['Date', 'Debit']]
    previous_debit = random.uniform(min_value, max_value)
    while current_date <= end_date:
        formatted_date = current_date.strftime('%Y-%m-%d')

        # Ajouter une variation réaliste
        daily_variation = random.uniform(-0.3, 0.3)
        current_debit = max(min(previous_debit + daily_variation, max_value), min_value)

        data.append([formatted_date, current_debit])

        previous_debit = current_debit
        current_date += timedelta(days=1)

    return data

# Définir la plage de dates et les valeurs minimales et maximales de débit
start_date = datetime(1969, 1, 1)
end_date = datetime(2022, 12, 31)
min_value = 0.8  # Débit minimal en mètres cubes par seconde
max_value = 8  # Débit maximal en mètres cubes par seconde

# Générer les données
river_flow_data = generate_river_flow_data(start_date, end_date, min_value, max_value)

# Écrire les données dans un fichier CSV
csv_filename = 'data/river_flow_data.csv'
with open(csv_filename, 'w', newline='') as csvfile:
    csv_writer = csv.writer(csvfile)
    csv_writer.writerows(river_flow_data)

print(f'Fichier CSV généré avec succès : {csv_filename}')
