from pathlib import Path
# noinspection PyPep8Naming
from dataclasses import dataclass
import xml.etree.ElementTree as ET
from typing import List
from datetime import datetime

import flask

HISTORY_FOLDER = Path('./history')


def get_real_path(name):
    p = HISTORY_FOLDER / name
    if not p.exists():
        p = HISTORY_FOLDER / (FAVORITE_PREFIX + name)
        if not p.exists():
            raise MissingSimulationError
        return p, True
    return p, False

def get_real_name(name):
    if name.startswith(FAVORITE_PREFIX):
        return name[len(FAVORITE_PREFIX):]
    return name


def delete_simulation(name: str):
    simulation_folder, _ = get_real_path(name)
    for f in simulation_folder.iterdir():
        f.unlink()
    simulation_folder.rmdir()


def rename_simulation(name: str, new_name: str):
    simulation_folder, fav = get_real_path(name)
    simulation_folder.rename(HISTORY_FOLDER / ((FAVORITE_PREFIX if fav else '') + new_name))


def set_favorite(name: str):
    simulation_folder, _ = get_real_path(name)
    simulation_folder.rename(HISTORY_FOLDER / (FAVORITE_PREFIX + name))


def unset_favorite(name: str):
    simulation_folder, _ = get_real_path(name)
    simulation_folder.rename(HISTORY_FOLDER / name)


def get_xml(name):
    simulation_folder, _ = get_real_path(name)
    return next(simulation_folder.glob('*.xml'))


def get_csv(name):
    simulation_folder, _ = get_real_path(name)
    return next(simulation_folder.glob('*.csv'))


@dataclass
class Property:
    name: str
    value: float or str


@dataclass
class Simulation:
    name: str
    date: str
    favorite: bool
    properties: List[Property]

    def __init__(self, name: str, date: str, property_name: str = None, property_value: str = None):
        self.favorite = name.startswith(FAVORITE_PREFIX)
        self.name = get_real_name(name)
        self.date = date
        self.properties = []
        if property_name is not None and property_value is not None:
            self.properties.append(Property(property_name, convert_type(property_value)))


    def __getitem__(self, key):
        return next(filter(lambda x: x.name == key, self.properties)).value if key not in ['name', 'date', 'favorite'] else getattr(self, key)

    def __repr__(self):
        return f"Simulation(name={self.name}, date={self.date}, favorite={self.favorite}, property_name={self.property_name}, property_value={self.property_value})"



@dataclass
class Filter:
    field: str
    operator: str
    value: str


op = {
    '=': lambda x, y: x == y,
    '!=': lambda x, y: x != y,
    '>': lambda x, y: x > y,
    '<': lambda x, y: x < y,
    '>=': lambda x, y: x >= y,
    '<=': lambda x, y: x <= y,
}


def convert_type(value: str) -> float or str:
    try:
        try:
            return int(value)
        except:
            return float(value)
    except:
        return value

def get_date(directory: Path):
    return datetime.fromtimestamp(directory.stat().st_mtime).strftime('%Y-%m-%d %H:%M:%S')

def find_value(xml_file: Path, xml_element_path: str):
    if xml_element_path == 'name':
        return get_real_name(xml_file.parent.name)
    if xml_element_path == 'date':
        return get_date(xml_file.parent)

    path_elements = xml_element_path.split('.')
    current_element = ET.parse(xml_file).getroot()

    for i, element_name in enumerate(path_elements):
        # Search for the current element among the sub-elements of the current element
        xml_name = 'ParametersGroup' if i != len(path_elements) - 1 else 'Parameter'

        current_element = current_element.find(f"./{xml_name}[@name='{element_name}']")
        # Check if the current element has been found
        if current_element is None:
            return None

    # If we have reached the end of the path, retrieve the value of the final element
    value_element = current_element.find('value')

    if value_element is not None:
        return value_element.text
    else:
        return None


def filter_xml(xml: Path, filters: [Filter]):
    for filter in filters:
        if filter['field'] == '' or filter['operator'] == '':
            return False
        if not op[filter['operator']](convert_type(find_value(xml, filter['field'])), convert_type(filter['value'])):
            return False
    return True


FAVORITE_PREFIX = 'FAV_'


def get_available_properties(xml: Path):
    return generate_path_list(ET.parse(xml).getroot())


def generate_path_list(node, parent_path=''):
    paths = []

    for child in list(node):
        tag_name = child.get('name', '')
        current_path = f"{parent_path}.{tag_name}" if parent_path else tag_name

        if list(child):
            # If the node has children, recurse to explore subgroups
            paths.extend(generate_path_list(child, current_path))
        else:
            # If the node is a Parameter, add the path to the list
            paths.append(current_path[:-1])

    return paths


class MissingSimulationError(Exception):
    pass
